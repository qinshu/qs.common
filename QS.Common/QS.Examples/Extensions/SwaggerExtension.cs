﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace QS.Examples.Extensions
{
    public static class SwaggerExtension
    {
        public static void AddSwaggerExtension(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            #region Swagger
            var ApiName = "公共工具类示例";
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    // {ApiName} 定义成全局变量，方便修改
                    Version = "v1",
                    Title = $"{ApiName} 接口文档",
                    Description = $"{ApiName} HTTP API V1",
                    Contact = new OpenApiContact { Name = ApiName, Email = "hushou0328@foxmail.com", Url = new Uri("https://www.cnblogs.com/fgq520328/") },
                    License = new OpenApiLicense { Name = ApiName, Url = new Uri("https://www.cnblogs.com/fgq520328/") }
                });
                c.OrderActionsBy(o => o.RelativePath);

                var xmlPath = string.Format("{0}/QS.Examples.XML", System.AppDomain.CurrentDomain.BaseDirectory);
                c.IncludeXmlComments(xmlPath, true);//默认的第二个参数是false，这个是controller的注释，记得修改

                c.OperationFilter<AddResponseHeadersFilter>();
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();

                c.OperationFilter<SecurityRequirementsOperationFilter>();

            });
            #endregion
        }
    }
}
